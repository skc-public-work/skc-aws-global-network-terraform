.PHONY: test_env 



include .env
export

BASE_DIR = $(dir $(shell pwd))
DIR = $(notdir $(shell pwd))

test_env:
	@echo $(AWS_ACCESS_KEY_ID)
	@echo $(AWS_SECRET_ACCESS_KEY)
	@echo $(AWS_DEFAULT_REGION)
	@echo $(AWS_DEFAULT_OUTPUT)


test:
	echo $(DIR)


# make tr cmd='-version'
cmd:
	terraform $(cmd) 


init:
	terraform init

validate:
	terraform validate

fmt:
	terraform fmt

plan:
	terraform plan

apply:
	terraform apply

destroy:
	terraform apply -destroy

state_list:
	terraform state list

output:
	terraform output

run_precess_ip:
	terraform output | ./process_ip_address.py

list_instances:
	PAGER=cat aws ec2 describe-instances  --output json