<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [skc-aws-global-network-terraform](#skc-aws-global-network-terraform)
- [setup S3 bucket for the state file](#setup-s3-bucket-for-the-state-file)

<!-- markdown-toc end -->




# skc-aws-global-network-terraform

This is based on 

OREILLY class AWS Global Networking in Terraform
https://learning.oreilly.com/live-events/aws-global-networking-in-terraform/0636920066438/0790145053178/

# state file and lock file

## setup S3 bucket for the state file

`skc-terrafrom-globalnet-state-file` for state file

## setup lock file in dynamodb
`terraform-state-locking`
